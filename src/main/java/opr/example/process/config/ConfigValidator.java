package opr.example.process.config;

import java.util.HashSet;
import java.util.Set;

class ConfigValidator {

	private ProcessesConfig config;

	ConfigValidator(ProcessesConfig config) {
		this.config = config;
	}

	void validate() {
		checkForDuplicitProcessNames();
	}

	private void checkForDuplicitProcessNames() {
		Set<String> names = new HashSet<>();
		for (ProcessConfig proc : config.getProcessesList()) {
			if (names.contains(proc.getName())) {
				throw new InvalidConfigurationException("Configuration contains duplicate process names: " + proc.getName());
			}
			names.add(proc.getName());
		}
	}

}
