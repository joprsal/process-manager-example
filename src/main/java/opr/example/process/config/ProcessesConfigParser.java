package opr.example.process.config;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;


public class ProcessesConfigParser {
	
	/**
	 * Parses a processes configuration file into the {@link ProcessesConfig} object.
	 */
	public ProcessesConfig parseFrom(File processesConfigXml) {
		try {
			ProcessesConfig config = parseConfigFile(processesConfigXml);
			new ConfigValidator(config).validate();
			return config;
		} catch (JAXBException e) {
			throw new RuntimeException("Invalid config XML: " + processesConfigXml, e);
		}
	}

	private ProcessesConfig parseConfigFile(File processesConfigXml) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(ProcessesConfig.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (ProcessesConfig) jaxbUnmarshaller.unmarshal(processesConfigXml);
	}
}
