package opr.example.process.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD)
public class ProcessConfig {

	@XmlAttribute(name="name")
	private String name;
	
	@XmlElement(name="workdir")
	private String workdir;
	
	@XmlElement(name="command")
	private String cmd;

	@XmlElement(name="outputfile")
	private String outputFile;
	
	
	public ProcessConfig() {
	}
	
	public ProcessConfig(String name, String workDir, String cmd, String outputFile) {
		this.name = name;
		this.workdir = workDir;
		this.cmd = cmd;
		this.outputFile = outputFile;
	}

	
	public String getName() {
		return name;
	}

	public String getCmd() {
		return cmd;
	}

	public String getWorkDir() {
		return workdir;
	}

	public String getOutputFile() {
		return outputFile;
	}
}
