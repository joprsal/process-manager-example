package opr.example.process.config;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="processes")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProcessesConfig {

	@XmlElement(name="process")
	private List<ProcessConfig> processesList;

	
	public ProcessesConfig() {
	}

	
	public List<ProcessConfig> getProcessesList() {
		return processesList;
	}

}
