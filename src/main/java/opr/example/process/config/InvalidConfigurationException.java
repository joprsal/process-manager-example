package opr.example.process.config;

/**
 * Indicates that process configuration is not valid.
 */
public class InvalidConfigurationException extends RuntimeException {
	private static final long serialVersionUID = 8609534992162824371L;

	public InvalidConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidConfigurationException(String message) {
		super(message);
	}
}
