package opr.example.process.manager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.exec.LogOutputStream;


class FileLoggingOutputStream extends LogOutputStream {

	private final String procName;
	private final File logFile;

	private FileOutputStream fileOut;

	
	FileLoggingOutputStream(String procName, File logFile) {
		this.procName = procName;
		this.logFile = logFile;
	}
	
	
	@Override
	protected void processLine(String line, int logLevel) {
		System.out.println(String.format("[%s] %s", procName, line));
		writeLineToFile(line);
	}

	synchronized
	private void writeLineToFile(String line) {
		try {
			openFileOutputStreamIfNeeded();
			fileOut.write(line.getBytes());
			fileOut.write("\n".getBytes());
		} catch (IOException e) {
			//TODO disable file writing
			e.printStackTrace();
		}
	}

	private void openFileOutputStreamIfNeeded() throws IOException {
		if (fileOut == null) {
			fileOut = new FileOutputStream(logFile, true);
		}
	}

}
