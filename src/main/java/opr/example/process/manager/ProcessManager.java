package opr.example.process.manager;

import java.io.File;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.Executor;

import com.google.common.annotations.VisibleForTesting;

import opr.example.process.config.ProcessConfig;
import opr.example.process.config.ProcessesConfig;


/**
 * Given a list of configured processes, creates a new instance of {@link SingleProcess} for each one.
 */
public class ProcessManager {

	private final ProcessesConfig config;

	
	public ProcessManager(ProcessesConfig config) {
		this.config = config;
	}

	
	public void start() {
		for (ProcessConfig proc : config.getProcessesList()) {
			startSingleProcess(proc);
		}
	}

	private void startSingleProcess(ProcessConfig procConfig) {
		SingleProcess proc = newProcess(
				new DefaultExecutor(),
				procConfig.getName(),
				extractWorkDirFrom(procConfig),
				extractCmdLineFrom(procConfig),
				extractOutputFileFrom(procConfig));
		proc.start();
	}

	@VisibleForTesting
	SingleProcess newProcess(Executor executor, String procName, File workDir, CommandLine cmdLine, File outputDir) {
		return new SingleProcess(executor, procName, workDir, cmdLine, outputDir);
	}

	private File extractWorkDirFrom(ProcessConfig procConfig) {
		String workDir = procConfig.getWorkDir();
		return (workDir == null) ? null : new File(workDir);
	}

	private CommandLine extractCmdLineFrom(ProcessConfig procConfig) {
		String cmdString = procConfig.getCmd();
		return CommandLine.parse(cmdString);
	}

	private File extractOutputFileFrom(ProcessConfig procConfig) {
		return new File(procConfig.getOutputFile());
	}
}
