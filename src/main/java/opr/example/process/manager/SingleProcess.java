package opr.example.process.manager;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;


/**
 * Manages a single external process.
 * 1. upon call to {@link SingleProcess#start()}, launches the process
 * 2. when the process completes, restarts it
 * 3. destroys the process at JVM shutdown
 * 
 * If the process fails to start (e.g. due to invalid command), it will be ignored.
 */
class SingleProcess {

	private final Executor cmdExecutor;
	private final String procName;
	private final File workDir;
	private final CommandLine cmdLine;
	private final File outputFile;
	private final ProcessRestartHandler restartHandler;

	
	SingleProcess(Executor cmdExecutor, String procName, File workDir, CommandLine cmdLine, File outputFile) {
		this.cmdExecutor = cmdExecutor;
		this.procName = procName;
		this.workDir = workDir;
		this.cmdLine = cmdLine;
		this.outputFile = outputFile;
		this.restartHandler = new ProcessRestartHandler(procName, this::start);
		setupWorkingDir();
		setupShutdownHook();
		setupStreamHandlerWithName();
	}
	
	private void setupWorkingDir() {
		cmdExecutor.setWorkingDirectory(workDir);
	}

	private void setupShutdownHook() {
		cmdExecutor.setProcessDestroyer(new ShutdownHookProcessDestroyer());
	}

	private void setupStreamHandlerWithName() {
//		OutputStream logOutputStream = new LogOutputStream() {
//			@Override
//			protected void processLine(String line, int logLevel) {
//				System.out.println(String.format("[%s] %s", procName, line));
//			}
//		};
		OutputStream logOutputStream = new FileLoggingOutputStream(procName, outputFile);
		ExecuteStreamHandler streamHandler = new PumpStreamHandler(logOutputStream);
		cmdExecutor.setStreamHandler(streamHandler);
	}


	void start() {
		try {
			cmdExecutor.execute(cmdLine, restartHandler);
			System.out.println("Started process ["+procName+"]");
		} catch (IOException e) {
			System.out.println("Failed to start process ["+procName+"]: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
