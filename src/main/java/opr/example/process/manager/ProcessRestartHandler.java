package opr.example.process.manager;

import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;

import com.google.common.annotations.VisibleForTesting;


/**
 * Implementation of {@link ExecuteResultHandler} that will call a {@link ProcessRestartHook}
 * when notified about process completion or failure.
 */
class ProcessRestartHandler implements ExecuteResultHandler {
	
	static final long DEFAULT_RESTART_DELAY = 1000L;

	interface ProcessRestartHook {
		void restart();
	}
	
	
	private final String procName;
	private final ProcessRestartHook restartHook;
	private final long restartDelayMillis;


	public ProcessRestartHandler(String procName, ProcessRestartHook restartHook) {
		this(procName, restartHook, DEFAULT_RESTART_DELAY);
	}
	
	@VisibleForTesting
	ProcessRestartHandler(String procName, ProcessRestartHook restartHook, long restartDelayMillis) {
		this.procName = procName;
		this.restartHook = restartHook;
		this.restartDelayMillis = restartDelayMillis;
	}


	@Override
	public void onProcessComplete(int exitValue) {
		printOut("Process [%s] completed with exit code %d", procName, exitValue);
		callRestartHookAfterDelayAsynchronously();
	}

	@Override
	public void onProcessFailed(ExecuteException e) {
		printOut("Process [%s] failed (%d): %s", procName, e.getExitValue(), e.getMessage());
		e.printStackTrace();
		callRestartHookAfterDelayAsynchronously();
	}

	
	private void callRestartHookAfterDelayAsynchronously() {
		Thread processRestartThread = new Thread(new Runnable() {
			@Override
			public void run() {
				callRestartHookAfterDelay();
			}
		});
		processRestartThread.setDaemon(true);
		processRestartThread.setName(String.format("Restart thread for [%s]", procName));
		processRestartThread.start();
	}

	private void callRestartHookAfterDelay() {
		printOut("Will restart process [%s] in %d millis...", procName, restartDelayMillis);
		sleepBeforeRestart();
		restartHook.restart();
	}

	private void sleepBeforeRestart() {
		try {
			Thread.sleep(restartDelayMillis);
		} catch (InterruptedException e) {
			throw new RuntimeException("thread interrupted", e);
		}
	}

	private void printOut(String formatString, Object... args) {
		System.out.println(String.format(formatString, args));
	}

}
