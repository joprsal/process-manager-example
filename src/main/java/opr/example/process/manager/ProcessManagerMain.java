package opr.example.process.manager;

import java.io.File;
import java.io.IOException;

import opr.example.process.config.ProcessesConfig;
import opr.example.process.config.ProcessesConfigParser;

/**
 * Entry point for starting the process manager.
 * Method main() expects a single argument holding the path to a configuration file.
 */
public class ProcessManagerMain {

	public static void main(String[] args) throws Exception {
		File configXml = new File(args[0]);
		new ProcessManagerMain(configXml).start();
		waitUntilKeyPressed();
		System.out.println("Key pressed; Going to terminate");
	}
	
	private static void waitUntilKeyPressed() throws IOException {
		System.out.println("Waiting for key pressed before terminating...");
		System.in.read();
	}


	private static final ProcessesConfigParser CONFIG_PARSER = new ProcessesConfigParser();

	private final ProcessManager processManager;
	
	public ProcessManagerMain(File configXml) {
		ProcessesConfig config = CONFIG_PARSER.parseFrom(configXml);
		processManager = new ProcessManager(config);
	}

	public void start() {
		processManager.start();
	}
}
