package opr.example.process.manager;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.concurrent.CountDownLatch;

import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.common.base.Stopwatch;

import opr.example.process.config.ProcessConfig;
import opr.example.process.manager.ProcessRestartHandler.ProcessRestartHook;
import opr.example.process.util.SystemStreamsRecorder;


public class ProcessRestartHandlerTest {

	private static final String PROC_NAME = "test proc";
	private static final ExecuteException FAKE_EXCEPTION = new ExecuteException("fake error", -1);
	
	@Rule
	public Timeout timeoutRule = new Timeout(1000L, MILLISECONDS);
	@Rule
	public SystemStreamsRecorder systemStreamsRecorder = new SystemStreamsRecorder();
	
	@Mock private ProcessRestartHook mockRestartHook;
	@Mock private ProcessConfig mockProcess;

	private final Stopwatch restartDelayStopwatch = Stopwatch.createUnstarted();
	private CountDownLatch restartTriggeredLatch;
	
	private ProcessRestartHandler processRestartHandler;

	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		when(mockProcess.getName()).thenReturn(PROC_NAME);
		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				restartDelayStopwatch.stop();
				restartTriggeredLatch.countDown();
				return null;
			}
		}).when(mockRestartHook).restart();
		startRestartDelayStopwatch();
	}

	private void startRestartDelayStopwatch() {
		restartDelayStopwatch.reset();
		restartDelayStopwatch.start();
		restartTriggeredLatch = new CountDownLatch(1);
	}
	
	
	@Test
	public void should_implement_ExecuteResultHandler() {
		setupRestartHandler();
		assertThat(processRestartHandler, instanceOf(ExecuteResultHandler.class));
	}
	
	@Test
	public void should_not_call_restart_hook_when_no_process_completed_or_failed() {
		setupRestartHandler();
		verify(mockRestartHook, never()).restart();
	}
	
	@Test
	public void should_call_restart_hook_when_process_completes() throws Exception {
		setupRestartHandler();

		processRestartHandler.onProcessComplete(0);
		restartTriggeredLatch.await();
		
		verify(mockRestartHook, times(1)).restart();
	}
	
	@Test
	public void should_call_restart_hook_when_process_fails() throws Exception {
		setupRestartHandler();

		processRestartHandler.onProcessFailed(FAKE_EXCEPTION);
		restartTriggeredLatch.await();
		
		verify(mockRestartHook, times(1)).restart();
	}
	
	@Test
	public void should_print_message_when_process_completes() {
		setupRestartHandler();
		processRestartHandler.onProcessComplete(123);
		assertStdoutContains(PROC_NAME);
		assertStdoutContains("completed");
		assertStdoutContains("123");
	}
	
	@Test
	public void should_print_message_when_process_fails() {
		setupRestartHandler();
		processRestartHandler.onProcessFailed(FAKE_EXCEPTION);
		assertStdoutContains(PROC_NAME);
		assertStdoutContains("failed");
		assertStdoutContains(String.valueOf(FAKE_EXCEPTION.getExitValue()));
		assertStdoutContains(FAKE_EXCEPTION.getMessage());
	}
	
	@Test
	public void should_delay_before_calling_restart_hook() throws Exception {
		long RESTART_DELAY_MILLIS = 50L;
		setupRestartHandler(RESTART_DELAY_MILLIS);

		processRestartHandler.onProcessComplete(0);
		restartTriggeredLatch.await();
		
		long delayMillis = restartDelayStopwatch.elapsed(MILLISECONDS);
		assertThat(delayMillis, greaterThanOrEqualTo(RESTART_DELAY_MILLIS));
	}

	
	private void setupRestartHandler() {
		setupRestartHandler(0L);
	}
	
	private void setupRestartHandler(long restartDelayMillis) {
		processRestartHandler = new ProcessRestartHandler(PROC_NAME, mockRestartHook, restartDelayMillis);
	}

	
	private void assertStdoutContains(String expectedSubstring) {
		String stdoutStr = systemStreamsRecorder.getStdout();
		assertThat(stdoutStr, containsString(expectedSubstring));
	}

}
