package opr.example.process.manager;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.apache.commons.exec.ExecuteWatchdog.INFINITE_TIMEOUT;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.lang.management.ManagementFactory;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.Timeout;

import opr.example.process.IntegrationTest;
import opr.example.process.util.SleepingProgram;
import opr.example.process.util.SystemStreamsRecorder;
import opr.example.process.util.TempFileCreator;


@Category(IntegrationTest.class)
public class ProcessManagerMainTest {

	private static final String PROC_NAME = "test_proc";
	
	@Rule
	public Timeout timeout = new Timeout(1000L, MILLISECONDS);
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	@Rule
	public SystemStreamsRecorder systemStreamRecorder = new SystemStreamsRecorder();

	private DefaultExecutor mainExecutor;
	private DefaultExecutor grepExecutor;
	
	private TempFileCreator fileCreator = new TempFileCreator(folder);
	private ProcessManagerMain processManagerMain;
	private boolean isMainExecutorStarted;

	private OutputStream outToMainStdin;
	private InputStream mainStdin;
	private DefaultExecuteResultHandler mainExecuteResultHandler;

	
	@Before
	public void init() throws IOException {
		mainExecutor = new DefaultExecutor();
		mainExecutor.setProcessDestroyer(new ShutdownHookProcessDestroyer());
		mainExecutor.setWatchdog(new ExecuteWatchdog(INFINITE_TIMEOUT));
		setupOutputToMainStdin();

		grepExecutor = new DefaultExecutor();
		grepExecutor.setProcessDestroyer(new ShutdownHookProcessDestroyer());
	}

	private void setupOutputToMainStdin() throws IOException {
		OutputStream logOutputStream = new LogOutputStream() {
			@Override
			protected void processLine(String line, int logLevel) {
				System.out.println(String.format("[main] %s", line));
			}
		};
		outToMainStdin = new PipedOutputStream();
		mainStdin = new PipedInputStream((PipedOutputStream) outToMainStdin);
		ExecuteStreamHandler streamHandler = new PumpStreamHandler(logOutputStream, logOutputStream, mainStdin);
		mainExecutor.setStreamHandler(streamHandler);
	}
	
	@After
	public void cleanup() {
		if (isMainExecutorStarted) {
			mainExecutor.getWatchdog().destroyProcess();
		}
	}
	
	
	@Test
	public void should_launch_configured_process_when_instance_starts() throws Exception {
		final String CMD = buildJavaCommand(SleepingProgram.class, "1000");
		
		File configXml = createSingleProcessConfigFileFor(CMD);
		processManagerMain = new ProcessManagerMain(configXml);
		processManagerMain.start();
		waitUntilStdoutContains("["+PROC_NAME+"]");

		assertProcessRunning(CMD);
	}
	
	@Test
	public void should_boot_main_as_standard_java_program() throws Exception {
		File configXml = createSingleProcessConfigFileFor("echo abc");
		String bootCmd = launchProcessManagerMain(configXml);
		
		assertProcessRunning(bootCmd);
	}
	
	@Test
	public void should_launch_configured_process_when_main_boots() throws Exception {
		File configXml = createSingleProcessConfigFileFor("echo main_started_ok");
		launchProcessManagerMain(configXml);
		
		waitUntilStdoutContains("main_started_ok");
	}
	
	@Test
	public void should_terminate_main_when_key_is_pressed() throws Exception {
		File configXml = createSingleProcessConfigFileFor("echo abc");
		String bootCmd = launchProcessManagerMain(configXml);
		
		assertProcessRunning(bootCmd);
		
		sendKeyPressed();

		mainExecuteResultHandler.waitFor();
		assertProcessNotRunning(bootCmd);
	}

	@Test
	public void should_work_if_main_is_invoked_directly() throws Exception {
		File configXml = createSingleProcessConfigFileFor("echo main_started_ok");

		invokeMainDirectly(configXml);
		
		waitUntilStdoutContains("main_started_ok");
	}

	private void invokeMainDirectly(File configXml) throws IOException, InterruptedException {
		final CountDownLatch latch = new CountDownLatch(1);
		new Thread(new Runnable() {
			@Override
			public void run() {
				InputStream originalIn = System.in;
				System.setIn(new ByteArrayInputStream("terminate now\n".getBytes()));
				try {
					ProcessManagerMain.main(new String[] {configXml.getAbsolutePath()});
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					System.setIn(originalIn);
					latch.countDown();
				}
			}
		}).start();
		sendKeyPressed();
		latch.await();
	}

	
	private void sendKeyPressed() throws IOException {
		outToMainStdin.write("terminate\n".getBytes());
		outToMainStdin.close();
	}

	private String launchProcessManagerMain(File configXml) throws Exception {
		String bootCmd = buildJavaCommand(ProcessManagerMain.class, configXml.getAbsolutePath());
		mainExecuteResultHandler = new DefaultExecuteResultHandler();
		mainExecutor.execute(CommandLine.parse(bootCmd), mainExecuteResultHandler);
		waitUntilStdoutContains("["+PROC_NAME+"]");
		isMainExecutorStarted = true;
		return bootCmd;
	}

	private void waitUntilStdoutContains(String expectedStdoutContent) throws InterruptedException {
		while (true) {
			if (systemStreamRecorder.getStdout().contains(expectedStdoutContent)) {
				break;
			}
			Thread.sleep(10L);
		}
	}


	private void assertProcessRunning(String expectedProcessCommand) throws Exception {
		List<String> outputLines = grepForProcess(expectedProcessCommand);
		assertThat(outputLines.size(), is(1));
	}

	private void assertProcessNotRunning(String expectedProcessCommandNotRunning) throws Exception {
		List<String> outputLines = grepForProcess(expectedProcessCommandNotRunning);
		assertThat(outputLines.size(), is(0));
	}


	private List<String> grepForProcess(String expectedProcessCommand) throws ExecuteException, IOException {
		final List<String> outputLinesCollector = new CopyOnWriteArrayList<>();
		OutputStream stdoutRecordingOutputStream = new LogOutputStream() {
			@Override
			protected void processLine(String line, int logLevel) {
				outputLinesCollector.add(line);
				System.out.println("[grep] " + line);
			}
		};
		grepExecutor.setStreamHandler(new PumpStreamHandler(stdoutRecordingOutputStream));
		
		int exitCode = grepExecutor.execute(grepForProcessCommand(expectedProcessCommand));
		assertThat(exitCode, is(0));
		
		return outputLinesCollector;
	}

	private CommandLine grepForProcessCommand(String processCommand) {
		String searchCmd = "ps aux | grep -v grep | grep \""+processCommand+"\" | awk '{ print $2 }'";
		CommandLine cmdLine = new CommandLine("/bin/sh");
		cmdLine.addArgument("-c");
		cmdLine.addArgument(searchCmd, false);
		return cmdLine;
	}


	private File createSingleProcessConfigFileFor(String procCommand) throws IOException {
		return fileCreator.createFile(
				"config.xml",
				//
				"<processes>",
				"    <process name=\""+PROC_NAME+"\">",
				"        <workdir>.</workdir>",
				"        <command>"+procCommand+"</command>",
				"        <outputfile>"+PROC_NAME+".out</outputfile>",
				"    </process>",
				"</processes>");
	}

	private String buildJavaCommand(Class<?> mainClass, String args) {
		return String.format("%s -cp \"%s\" %s %s",
				getJava(),
				getCurrentClasspath(),
				mainClass.getName(),
				args);
	}
	
	private Object getJava() {
		return String.format("%s/bin/java", System.getProperty("java.home"));
	}

	private String getCurrentClasspath() {
		return ManagementFactory.getRuntimeMXBean().getClassPath();
	}
	
}
