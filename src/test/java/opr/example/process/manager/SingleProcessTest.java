package opr.example.process.manager;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.process.util.SystemStreamsRecorder;

public class SingleProcessTest {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	@Rule
	public SystemStreamsRecorder systemStreamsRecorder = new SystemStreamsRecorder();

	@Mock private Executor cmdExecutor;

	private String procName;
	private File workDir;
	private CommandLine cmdLine;
	private SingleProcess singleProcess;

	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	
	@Test
	public void should_execute_process_using_the_given_work_dir() throws Exception {
		final File WORK_DIR = new File(".");
		givenWorkDir(WORK_DIR);
		startSingleProcess();
		assertWorkDir(WORK_DIR);
	}
	
	@Test
	public void should_execute_process_using_the_given_command_line() throws Exception {
		final String CMD = "echo abc";
		givenCmdLine(CMD);
		startSingleProcess();
		assertCmdLine(CMD);
	}

	@Test
	public void should_allow_null_work_dir() throws Exception {
		givenWorkDir(null);
		startSingleProcess();
		assertWorkDir(null);
	}
	
	@Test
	public void should_install_shutdown_hook_process_destroyer() throws Exception {
		startSingleProcess();
		verify(cmdExecutor, times(1)).setProcessDestroyer(any(ShutdownHookProcessDestroyer.class));
	}
	
	@Test
	public void should_install_ProcessRestartHandler() throws Exception {
		startSingleProcess();
		verify(cmdExecutor, times(1)).execute(any(CommandLine.class), any(ProcessRestartHandler.class));
	}
	
	@Test
	public void should_print_process_when_started() throws Exception {
		givenProcessName("proc_A");
		startSingleProcess();
		systemStreamsRecorder.assertStdoutContains("Started process");
		systemStreamsRecorder.assertStdoutContains("proc_A");
	}

	@Test
	public void should_print_process_when_failed() throws Exception {
		givenProcessName("invalid_proc_A");
		doThrow(new IOException("invalid command"))
				.when(cmdExecutor).execute(any(CommandLine.class), any(ExecuteResultHandler.class));
		
		startSingleProcess();
		
		systemStreamsRecorder.assertStdoutContains("Failed to start process");
		systemStreamsRecorder.assertStdoutContains("invalid_proc_A");
		systemStreamsRecorder.assertStdoutContains("invalid command");
	}

	
	private void givenProcessName(String procName) {
		this.procName = procName;
	}
	
	private void givenWorkDir(File workDir) {
		this.workDir = workDir;
	}
	
	private void givenCmdLine(String cmd) {
		this.cmdLine = CommandLine.parse(cmd);
	}

	
	private void startSingleProcess() throws Exception {
		File outputFile = folder.newFile(procName+".out");
		singleProcess = new SingleProcess(cmdExecutor, procName, workDir, cmdLine, outputFile);
		singleProcess.start();
		assertNumberOfExecutedCommands(1);
	}


	private void assertWorkDir(File expectedWorkDir) throws ExecuteException, IOException {
		ArgumentCaptor<File> workDirArg = ArgumentCaptor.forClass(File.class);
		verify(cmdExecutor, atLeast(1)).setWorkingDirectory(workDirArg.capture());
		File actualWorkDir = workDirArg.getValue();
		assertThat(actualWorkDir, is(expectedWorkDir));
	}

	private void assertCmdLine(String expectedCmdLineStr) throws ExecuteException, IOException {
		CommandLine expectedCmdLine = CommandLine.parse(expectedCmdLineStr);
		ArgumentCaptor<CommandLine> cmdLineArg = ArgumentCaptor.forClass(CommandLine.class);
		verify(cmdExecutor, atLeast(1)).execute(cmdLineArg.capture(), any(ExecuteResultHandler.class));
		CommandLine actualCmdLine = cmdLineArg.getValue();
		assertThat(actualCmdLine.toString(), is(expectedCmdLine.toString()));
	}

	
	private void assertNumberOfExecutedCommands(int expectedNumberOfCommandsExecuted) throws Exception {
		verify(cmdExecutor, times(expectedNumberOfCommandsExecuted))
				.execute(any(CommandLine.class), any(ExecuteResultHandler.class));
	}
}
