package opr.example.process.manager;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.Executor;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.process.config.ProcessConfig;
import opr.example.process.config.ProcessesConfig;
import opr.example.process.util.SystemStreamsRecorder;


public class ProcessManagerTest {

	private static final String PROC_NAME = "test process";
	private static final String WORK_DIR = "proc/work/dir";

	private static int procIdCounter;

	@Rule
	public SystemStreamsRecorder systemStreamsRecorder = new SystemStreamsRecorder();

	@Mock private ProcessesConfig processesConfig;
	@Mock private Executor mockExecutor;
	
	private ProcessManager processManager;
	
	
	@Before
	public void init() throws Exception {
		MockitoAnnotations.initMocks(this);
		processManager = new ProcessManager(processesConfig) {
			@Override
			SingleProcess newProcess(Executor executor, String procName, File workDir, CommandLine cmdLine, File outputFile) {
				return super.newProcess(mockExecutor, procName, workDir, cmdLine, outputFile);
			}
		};
		processManager = spy(processManager);
	}

	
	@Test
	public void should_launch_single_process() throws Exception {
		givenProcesses(
				proc(PROC_NAME, WORK_DIR, "echo abc"));

		processManager.start();
		
		assertNumberOfSingleProcessInstances(1);
		assertSingleProcessName(PROC_NAME);
		assertSingleProcessWorkDir(WORK_DIR);
		assertSingleProcessCommand("echo abc");
	}


	@Test
	public void should_launch_two_processes() throws Exception {
		givenProcesses(
				proc("echo abc"),
				proc("echo -n def"));

		processManager.start();
		
		assertNumberOfSingleProcessInstances(2);
	}
	
	@Test
	public void should_set_default_working_dir_when_not_specified() throws Exception {
		givenProcesses(
				proc("process without workdir", null, "echo abc"));

		processManager.start();
		
		assertSingleProcessWorkDir(null);
	}
	


	private void givenProcesses(ProcessConfig... configuredProcesses) {
		when(processesConfig.getProcessesList()).thenReturn(Arrays.asList(configuredProcesses));
	}

	
	private void assertSingleProcessName(String expectedProcName) throws Exception {
		assertNumberOfSingleProcessInstances(1);
		verify(processManager, times(1))
				.newProcess(any(), eq(expectedProcName), any(), any(), any());
	}
	
	private void assertSingleProcessWorkDir(String expectedProcWorkDirPath) throws Exception {
		assertNumberOfSingleProcessInstances(1);
		File expectedProcWorkDir = (expectedProcWorkDirPath == null) ? null : new File(expectedProcWorkDirPath);
		verify(processManager, times(1))
				.newProcess(any(), any(), eq(expectedProcWorkDir), any(), any());
	}
	
	private void assertSingleProcessCommand(String expectedProcCmd) throws Exception {
		assertNumberOfSingleProcessInstances(1);
		
		ArgumentCaptor<CommandLine> cmdLineArg = ArgumentCaptor.forClass(CommandLine.class);
		verify(processManager, times(1))
				.newProcess(any(), any(), any(), cmdLineArg.capture(), any());
		
		CommandLine actualProcCmdLine = cmdLineArg.getValue();
		CommandLine expectedProcCmdLine = CommandLine.parse(expectedProcCmd);
		
		assertThat(actualProcCmdLine.toString(), is(expectedProcCmdLine.toString()));
	}
	
	private void assertNumberOfSingleProcessInstances(int expectedNumberOfSingleProcesses) throws Exception {
		verify(processManager, times(expectedNumberOfSingleProcesses))
				.newProcess(any(), any(), any(), any(), any());
	}
	
	
	private ProcessConfig proc(String processCmd) {
		return proc("proc"+(procIdCounter++), WORK_DIR, processCmd);
	}

	private ProcessConfig proc(String procName, String workDir, String processCmd) {
		return new ProcessConfig(procName, workDir, processCmd, procName+".out");
	}

}
