package opr.example.process.manager;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.google.common.io.Files;

import opr.example.process.util.SystemStreamsRecorder;

public class LoggingOutputStreamTest {

	private static final Charset UTF8 = Charset.forName("UTF-8");

	private static final String PROCE_NAME = "testProc";

	@Rule
	public SystemStreamsRecorder systemStreamsRecorder = new SystemStreamsRecorder();
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	
	private File logFile;
	private FileLoggingOutputStream os;


	
	@Before
	public void init() throws IOException {
		logFile = folder.newFile(PROCE_NAME + ".out");
		os = new FileLoggingOutputStream(PROCE_NAME, logFile);
	}

	
	
	@Test
	public void should_log_to_stdout() throws IOException {
		String TEST_MSG = "test message\n";
		os.write(TEST_MSG.getBytes());
		systemStreamsRecorder.assertStdoutContains(PROCE_NAME);
		systemStreamsRecorder.assertStdoutContains(TEST_MSG);
	}
	
	@Test
	public void should_log_to_file() throws IOException {
		String TEST_MSG = "test message\n";
		os.write(TEST_MSG.getBytes());
		String actualFileContents = Files.toString(logFile, UTF8);
		assertThat(actualFileContents, is(TEST_MSG));
	}

}
