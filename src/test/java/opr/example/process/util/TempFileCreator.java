package opr.example.process.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.junit.rules.TemporaryFolder;

import com.google.common.base.Joiner;
import com.google.common.io.Files;


/**
 * Can create files populated with some content.
 */
public class TempFileCreator {

	private static final Charset UTF8 = Charset.forName("UTF-8");

	private final TemporaryFolder tempFolder;
	
	
	public TempFileCreator(TemporaryFolder tempFolder) {
		this.tempFolder = tempFolder;
	}
	
	
	public File createFile(String fileName, String... lines) throws IOException {
		String contents = Joiner.on("\n").join(lines);
		File configXml = tempFolder.newFile(fileName);
		Files.write(contents, configXml, UTF8);
		return configXml;
	}
}
