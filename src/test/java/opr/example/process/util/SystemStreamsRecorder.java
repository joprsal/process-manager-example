package opr.example.process.util;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Rule;
import org.junit.rules.ExternalResource;


/**
 * JUnit {@link Rule} that records the stdout stream.
 */
public class SystemStreamsRecorder extends ExternalResource {

	private PrintStream originalStdout;
	private ByteArrayOutputStream stdout;

	@Override
	protected void before() throws Throwable {
		super.before();
		originalStdout = System.out;
		stdout = new ByteArrayOutputStream();
		System.setOut(new PrintStream(stdout));
	}

	@Override
	protected void after() {
		super.after();
		System.setOut(originalStdout);
		System.out.println(getStdout());
	}

	
	public String getStdout() {
		return stdout.toString();
	}
	
	public void assertStdoutContains(String expectedSubstring) {
		assertThat(getStdout(), containsString(expectedSubstring));
	}

}
