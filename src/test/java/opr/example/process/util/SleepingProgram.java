package opr.example.process.util;


public class SleepingProgram {
	
	public static void main(String[] args) {
		long delayMillis = Long.valueOf(args[0]);
		sleep(delayMillis);
	}
	
	private static void sleep(long delayMillis) {
		try {
			Thread.sleep(delayMillis);
		} catch (InterruptedException e) {
			throw new RuntimeException("thread interrupted", e);
		}
	}
}
