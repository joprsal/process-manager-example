package opr.example.process;

import org.junit.experimental.categories.Category;

/**
 * To be used as a JUnit {@link Category} to mark tests that integrate two or more components together.
 */
public interface IntegrationTest {
}
