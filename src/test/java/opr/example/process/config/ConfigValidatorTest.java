package opr.example.process.config;

import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

public class ConfigValidatorTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	private ConfigCreator configCreator = new ConfigCreator(folder);

	
	@Test
	public void should_validate_ok_when_valid_config() throws Exception {

		createAndParseConfigFile(
				"validProcessesConfig.xml",
				//
				"<processes>",
				"    <process name=\"proc1\">",
				"        <workdir>/home/jozef/proc1</workdir>",
				"        <command>greeter.sh -c 1</command>",
				"        <outputfile>out/proc1.txt</outputfile>",
				"    </process>",
				"    <process name=\"proc2\">",
				"	     <workdir>/home/jozef/proc2</workdir>",
				"        <command>bash -c \"echo Hello World > hello.txt\"</command>",
				"        <outputfile>out/proc2.txt</outputfile>",
				"    </process>",
				"</processes>");
	}
	
	@Test
	public void should_fail_when_duplicit_proces_names() throws Exception {

		expectedException.expect(InvalidConfigurationException.class);
		expectedException.expectMessage("duplicate process names");
		
		createAndParseConfigFile(
				"duplicitProcessesConfig.xml",
				//
				"<processes>",
				"    <process name=\"proc1\">",
				"        <workdir>/home/jozef/proc1</workdir>",
				"        <command>greeter.sh -c 1</command>",
				"        <outputfile>out/proc1.txt</outputfile>",
				"    </process>",
				"    <process name=\"proc1\">",
				"	     <workdir>/home/jozef/proc2</workdir>",
				"        <command>bash -c \"echo Hello World > hello.txt\"</command>",
				"        <outputfile>out/proc2.txt</outputfile>",
				"    </process>",
				"</processes>");
	}

	
	private void createAndParseConfigFile(String fileName, String... lines) throws IOException {
		configCreator.createAndParseConfigFile(fileName, lines);
	}
}
