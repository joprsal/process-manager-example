package opr.example.process.config;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;

import org.junit.rules.TemporaryFolder;

import opr.example.process.util.TempFileCreator;


public class ConfigCreator {

	private TemporaryFolder folder;
	private TempFileCreator fileCreator = new TempFileCreator(folder);
	private ProcessesConfigParser parser = new ProcessesConfigParser();


	public ConfigCreator(TemporaryFolder tmpFolder) {
		this.folder = tmpFolder;
		this.fileCreator = new TempFileCreator(tmpFolder);
		this.parser = new ProcessesConfigParser();
	}
	
	public ProcessesConfig createAndParseConfigFile(String fileName, String... lines) throws IOException {
		File configXml = fileCreator.createFile(fileName, lines);
		return parseConfigFile(configXml);
	}
	
	private ProcessesConfig parseConfigFile(File configXml) {
		ProcessesConfig config = parser.parseFrom(configXml);
		assertThat(config, is(notNullValue()));
		return config;
	}


}
