package opr.example.process.config;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;


public class ProcessesConfigParserTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	
	private ConfigCreator configCreator = new ConfigCreator(folder);
	private ProcessesConfig config;

	
	@Test
	public void should_parse_xml_when_valid() throws Exception {
		createAndParseConfigFile(
				"validProcessesConfig.xml",
				//
				"<processes>",
				"    <process name=\"proc1\">",
				"        <workdir>/home/jozef/proc1</workdir>",
				"        <command>greeter.sh -c 1</command>",
				"        <outputfile>out/proc1.txt</outputfile>",
				"    </process>",
				"    <process name=\"proc2\">",
				"	     <workdir>/home/jozef/proc2</workdir>",
				"        <command>bash -c \"echo Hello World > hello.txt\"</command>",
				"        <outputfile>out/proc2.txt</outputfile>",
				"    </process>",
				"</processes>");

		assertProcessCount(2);
		assertProcess(0, "proc1", "/home/jozef/proc1", "greeter.sh -c 1", "out/proc1.txt");
		assertProcess(1, "proc2", "/home/jozef/proc2", "bash -c \"echo Hello World > hello.txt\"", "out/proc2.txt");
	}
	
	@Test
	public void should_fail_when_xml_is_invalid() throws Exception {
		String XML_FILE_NAME = "invalidFormat.xml";
		
		expectedException.expect(RuntimeException.class);
		expectedException.expectMessage(XML_FILE_NAME);
		
		createAndParseConfigFile(
				XML_FILE_NAME,
				//
				"<processes>",
				"    <process>",
				"    not terminated...",
				"</processes>");
		assertProcessCount(0);
	}

	
	private void createAndParseConfigFile(String fileName, String... lines) throws IOException {
		config = configCreator.createAndParseConfigFile(fileName, lines);
	}


	private void assertProcessCount(int expectedProcessCount) {
		assertThat(config.getProcessesList().size(), is(expectedProcessCount));
	}


	private void assertProcess(
			int elementIdx,
			String expectedName,
			String expectedWorkDir,
			String expectedCmd,
			String expectedOutputFile) {
		
		ProcessConfig actualProcessConfig = config.getProcessesList().get(elementIdx);
		assertThat(actualProcessConfig.getName(), is(expectedName));
		assertThat(actualProcessConfig.getWorkDir(), is(expectedWorkDir));
		assertThat(actualProcessConfig.getCmd(), is(expectedCmd));
		assertThat(actualProcessConfig.getOutputFile(), is(expectedOutputFile));
	}


}
